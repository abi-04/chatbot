const natural = require('natural');
const express = require('express');
const cors = require('cors');
const wordnet = new natural.WordNet();
const app = express();
const port = 3000;
app.use(cors());

app.get("/", (request, response) => {
    response.sendFile(__dirname + "/index.html");
});
  
app.get("/wordbot/:word", (request, response) => {
    wordnet.lookup(request.params.word, function(results) {
        var wordMeaning = {};
        resultDef = [], resultSyn = [];
        results.forEach(function(result) {
            resultDef.push(result.def);
            resultSyn.push(result.synonyms);
            resultSyn.push("\n");
            console.log(result.synonyms);
          });
          wordMeaning.definition =  resultDef;
          wordMeaning.synonyms =  resultSyn;
        response.send(wordMeaning);
    });
});
  
app.listen(port, () => {
  console.log(`app listening on port ${port}`);
});